FlowRouter.route( '/tweet', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'tweet' } ); 
  },
  name: 'tweet'
});

FlowRouter.route( '/lastTweet', {
  action: function() {
    BlazeLayout.render( 'applicationLayout', { main: 'displayTweet' } ); 
  },
  name: 'lastTweet'
});