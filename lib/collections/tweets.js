import { Mongo } from 'meteor/mongo';
 
Tweets = new Mongo.Collection('tweets');

Meteor.methods({
  tweetInsert: function(tweet) {

    var tweetId = Tweets.insert(tweet);

    return {
      _id: tweetId
    };
  }
});