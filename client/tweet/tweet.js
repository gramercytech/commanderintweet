var tweetListObj = [];
tweetListObj['iran'] = [];
tweetListObj['iran']['title'] = "Iran violates nuclear deal";
tweetListObj['iran']['segments'] = [
	  			["Iran is playing with Fire!", "#1 in terror Iran has been formally put ON NOTICE--", "Just as I predicted, Iran broke the \"sacred\" deal."],
	  			["They don't see how \"kind\" President Obama was to them.", "Many people want me to say \"I told you so!\"", "Our country was humiliated."],
	  			["NOT ME!", "Obvious long ago!", "- I told you so!"],
	  			["#MAGA", "#ToldYouSo", "#ThanksObama"]
	  		];
	  	// };

tweetListObj['wapo'] = [];
tweetListObj['wapo']['title'] = "Washington Post Has Tax Returns";
tweetListObj['wapo']['segments'] = [
	  			["I am a great businessman but", "What kind of a country do we have where", "I won the Electoral College but now"],
	  			["the failing @Washingtonpost", "a reporter, who nobody ever heard of,", "the FAKE NEWS media"],
	  			["brings up tax returns again?", "is so unfair to me!", "is still protecting Crooked Hillary!"],
	  			["FAKE NEWS!", "FAKK NEWS!", "I will release after audit!"]
	  		];

tweetListObj['ginsburg'] = [];
tweetListObj['ginsburg']['title'] = "Ruth Bader Ginsburg Stepping Down";
tweetListObj['ginsburg']['segments'] = [
	  			["Very dumb Ruth Bader Ginsburg (SC)", "My least favorite Supreme Court Justice", "Incompetent anti-Trump judge"],
	  			["is finally packing it in!", "resigned like a dog.", "doesn't have the stamina for another term."],
	  			["Her mind is shot!", "Will Dems let me have my pick?", "I know what Supreme Court is!"],
	  			["RESPECT!", "#MAGA", "#ByeBye"]
	  		];

tweetListObj['impeachment'] = [];
tweetListObj['impeachment']['title'] = "House of Reps Drafted Articles Of Impeachment";
tweetListObj['impeachment']['segments'] = [
	  			["I just got big news that", "A very informed \"source\" told me", "Wouldn't it be a surprise if"],
	  			["the failing Dems", "cowardly Paul Ryan's cronies", "the so-called \"Congress\""],
	  			["think they can outsmart TRUMP.", "have no evidence!", "want to destroy America."],
	  			["Drain the swamp!", "Won't happen!", "TRUMP!"]
	  		];

tweetListObj['incompetent'] = [];
tweetListObj['incompetent']['title'] = "Merkel Called You An Incompetent Baby";
tweetListObj['incompetent']['segments'] = [
	  			["I have always been \"nice\" but", "No one has thicker skin than me but", "I am the best friend the German people ever had but"],
	  			["so-called Chancellor Angela Merkel", "Nasty Angela", "some woman on my TV"],
	  			["is a hater and a loser!", "is the worst thing to happen in all of German history!", "can't accept that I won the Electoral College (306)"],
	  			["Sad!", "Not good!", "Who cares?"]
	  		];

tweetListObj['missile'] = [];
tweetListObj['missile']['title'] = "North Korea Launched Ballistic Missile";
tweetListObj['missile']['segments'] = [
	  			["A certain total wack job", "Crazy or sick (delusional) Kim", "Korea -- the bad one --"],
	  			["must be very careful", "will stop \"playing\" the United States", "has to learn respect"],
	  			["-- if not we will get tough!", "or I will tell China on him!", "-- he's gone TOO FAR!"],
	  			["Bad!", "Not good!", "#WW3"]
	  		];
tweetListObj['indicting'] = [];
tweetListObj['indicting']['title'] = "Robert Mueller is Indicting You";
tweetListObj['indicting']['segments'] = [
          ["Very biased Robert (Bob) Mueller", "Bad (or sick) witch hunter", "Incompetant anti-Trump investigator"],
          ["thinks he can take down TRUMP!", "will fail like a dog.", "should be investigating Crooked H."],
          ["SEE YOU IN COURT!", "I have the best lawyers.", "(check out sex tape)"],
          ["Not Nice!", "We will MAGA!", "i want mommy"]
        ];
        

// tweetIds = ['iran', 'wapo', 'ginsburg', 'impeachment', 'incompetent', 'missile'];

Template.tweet.onCreated(function(){
	var timeinterval;
	this.sceneId = new ReactiveVar(false);
	this.sceneStage = new ReactiveVar('');
	this.composedTweet = new ReactiveVar('');
	this.finalTweet = new ReactiveVar(false);
	Session.set("t", 30);
});
Template.tweet.helpers({
	isHeader: function() {
		sceneStage = Template.instance().sceneStage.get();
		return (sceneStage == "confirmed" || sceneStage == "started" || sceneStage == "finished" || sceneStage == "select");
	},
  tweetList: function() {
  	tweetIds = [];
  	for(scene in tweetListObj) {
  		tweetIds.push(scene);
  	}

  	return tweetIds;
  },
  sectionTitle: function(id) {
  	if (tweetListObj[id])
  		return tweetListObj[id].title;
  },
  sceneId: function() {
	return Template.instance().sceneId.get();
  },
  sceneStage: function(stage) {
  	sceneStage = Template.instance().sceneStage.get()
  	if (stage == sceneStage)
	  	return true;
  	else
  		return false;
  },
  currentSegments: function() {
  	thisId = Template.instance().sceneId.get();
  	if (tweetListObj[thisId])
	  	return tweetListObj[thisId]['segments'];
  },
  indexPlusOne: function(segList, segment) {
  	return segList.indexOf(segment)+1;
  	// return Number(intVal)+1;
  },
  composedTweet: function() {
  	return Template.instance().composedTweet.get();
  },
  tweetButton: function() {
		$('.section-title.compose').hide();
  	return Template.instance().finalTweet.get();
  },
  t: function () {
  		tVal = Session.get("t");
	},
	tValue: function(){
		if (tVal === 0) {
			console.log('time is up')
			$('.composing-tweet').hide();
			$('.times-up').show();
			return "0"+String(tVal);
		} else {
			if (tVal < 10) {
  			return "0"+String(tVal);
			} else {
				return tVal;
			}
		}
	}

});
Template.tweet.events({
	'click .enter': function(event, template) {
		template.sceneStage.set('select');
		$('.section-title.compose').show();
	},
	'click .scene': function(event, template) {
		$('.scene').removeClass('chosen');
		$('#'+event.target.id).addClass('chosen');
		template.sceneId.set(event.target.id);
	},
	'click .confirm': function(event, template) {
		template.sceneStage.set('confirmed');
	},
	'click .start': function(event, template) {
		template.sceneStage.set('started');
		// var endtime = 'September 8 2015 14:50:30 UTC-0400';
		// var d;
		var d = new Date();
		// alert(d.getMinutes() + ':' + d.getSeconds()); //11:55
		d.setSeconds(d.getSeconds() + 30);
		timeinterval = setInterval(function() {
      // Meteor.call("getCurrentTime", function(error, result) {
        result = Date.parse(new Date());
        Session.set("time", result);
        var t = getTimeRemaining(d);
        Session.set("t", t['seconds']);
      // });
    }, 1000);
	},
	'click .clear-scene': function(event, template) {
		template.$(".selected").removeClass("selected");
		template.$(".chosen").removeClass("chosen");
		template.sceneStage.set('');
		template.sceneId.set(false);
		template.composedTweet.set('');
		template.finalTweet.set(false);
		clearInterval(timeinterval);
		Session.set("t", 30);
		$('.composing-tweet').show();
		$('.times-up').hide();
	},
	'change .segment-select': function(event, template) {
		var tweet = "";

		template.$('.segment-select').each(function(){
			tweet += $(this).find("option.seg-value:selected").text()+" ";
		});
		template.composedTweet.set(tweet);
	  	unsetVals = template.$('.segment-select').find("option.no-seg-value:selected").length;
	  	template.finalTweet.set(unsetVals == 0);
	},
	'click li.seg-value': function(event, template) {
		var tweet = "";
		var hideSegment;

		//mark segment and value as selected
		$(event.currentTarget).parent().children().removeClass('selected');
		$(event.currentTarget).addClass('selected');
		$(event.currentTarget).parent().addClass('selected');

		//generate tweet
		template.$('.segment-select.selected').each(function(){
			tweet += $(this).find(".seg-value.selected").find('p').html()+" ";
		});

		//grab count of items shown
		var segmentCount = $('.segment-select.selected').last().find('.seg-value').size();

		//hide current segments
		$('.segment-select.selected').last().find('.seg-value').each(function(index){
			var $this = $(this);

				$this.removeClass('slide-in');
				$this.addClass('slide-out');

			// setTimeout(function(){
			// 	$this.removeClass('slide-in');
			// 	$this.addClass('slide-out');
			// }, 300*index);
		});

		//after all hidden, hide entire seg div and slide in new segments
		setTimeout(function(){
			$('.segment-select.selected').hide();
			$('.segment-select').not('.selected').first().find('.seg-value').each(function(index){
				var $this = $(this);
				setTimeout(function(){
					$this.addClass('slide-in');
				}, 150*index);
			});

		}, (150*segmentCount)+150);

		//set full tweet var
		template.composedTweet.set(tweet);
		sectionCount = template.$('.segment-select').length;
		console.log(sectionCount)
	  	setVals = template.$('.segment-select.selected').length;
	  	template.finalTweet.set(setVals == sectionCount);
	},
	'click .send-tweet': function(event, template) {
    var date = new Date();
		tweet = {composedTweet: template.composedTweet.get(), createdAt: date};
		Meteor.call('tweetInsert', tweet, function(error,result) {
	      if (error)
	        return alert(error.reason);
		});
		template.sceneStage.set('finished');
	}
});

 function getTimeRemaining(endtime){
    var t = Date.parse(endtime) - Session.get('time');

    if(t <= 0)
      clearInterval(timeinterval);

    return {
      'seconds': t/1000
    };

  }
