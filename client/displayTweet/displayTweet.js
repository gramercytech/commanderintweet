Template.displayTweet.helpers({
  formatDate(date){
    var monthNames = ["January", "February", "March", "April", "May", "June", "July",
    "August", "September", "October", "November", "December"];
    var h =  date.getHours();
    var m = (date.getMinutes() < 10? '0' : '') + date.getMinutes();
    var thistime = (h > 12) ? (h-12 + ':' + m +' PM') : (h + ':' + m +' AM');
    var currentMonth = date.getMonth() + 1;
    var currentYear = date.getFullYear();
    date = monthNames[date.getMonth()] + ' ' + currentYear + ' ' + thistime;
    return date;
  },
  lastTweet: function() {
    last = Tweets.findOne({}, {sort: {createdAt: -1, limit: 1}});
    console.log(last);
    return last;
  }

});
